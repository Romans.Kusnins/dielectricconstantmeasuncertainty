#include<stdio.h>
#include<complex.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<omp.h>

//#include"MonteCarlo.h"
#include"PoleZero.h"

#ifdef MP
	#include<mpi.h>
#endif

#define cdouble _Complex double

#define PI 3.141592653589793116


char path[100] 		= "./poles_zeros/"; 
char pathpoly[100] 	= "./poly_coeff/"; 


int main(int argc,char * argv[]){

	cdouble RT[2];

	int numprocs=1.0, rank=0.0, namelen, id, provided;

	al 		= AL;
	double r1 	= RAD;
	cdouble eps 	= 10.0;

#ifdef MP

	char procname[MPI_MAX_PROCESSOR_NAME];
	MPI_Status status;
	MPI_Init_thread(&argc,&argv,MPI_THREAD_SERIALIZED,&provided);
	MPI_Comm_size(MPI_COMM_WORLD,&numprocs); 
	MPI_Comm_rank(MPI_COMM_WORLD,&rank); 
	MPI_Get_processor_name(procname,&namelen);

#endif

	NUM0 = static_cast<int>(floor((r1-0.001)/dr));

	POLES_OPEN(al)
	POLES_READ(al)
	POLES_CLOSE

	POLY_OPEN(al)
	POLY_READ(al)
	POLY_CLOSE

	kor = 2.0*PI*al*r1;

//---------------------------------------------------------------------------------
	besselj(0, kor, BJo, 2*M+1);
	bessely(0, kor, BYo, 2*M+1);

	besscyl_freq(al, PQPt, M, IP);
//---------------------------------------------------------------------------------
	cdouble RC;

	int ida = 0;
	for(eps=1.0; creal(eps)<120.0; eps+=0.01){
		++ida;		
	}
	cdouble RT_vec[ida];
	ida = 0;
/*	
	for(eps=1.0; creal(eps)<1200.0; eps+=0.01){

        	cylindrRT(eps+I*0.0, r1, al, BJPt, PQPt, RT, M);

#ifdef POSITIVE
		RC = (RT[0]+RT[1])/fapp_2(eps+I*0.0, 0.0, 0.0, lpPt, NPOLE);
#else
		RC = (RT[0]-RT[1])/fapp_2(eps+I*0.0, 0.0, 0.0, lnPt, NPOLE);
#endif

//		printf("R+T=%5.12g,%5.12g\n", RC);

		RT_vec[ida] = RC;
		
//		RC = polynom(eps+I*0.0, 0.0, 0.0, rpPt, ORD);
//		printf("R+T=%5.12g,%5.12g\n\n", RC);
		++ida;		

	}
	printf("%d\n",ida);
*/
	eps = 100.0;

	printf("eps = %5.12f\n", eps);

       	cylindrRT(eps, r1, al, BJPt, PQPt, RT, M);

	RC = (RT[0]+RT[1]);
	printf("ACTUAL: R+T=%5.12g,%5.12g\n", RC);
	RC = polynom(eps, 0.0, 0.0, rpPt, ORD)*fapp_2(eps, 0.0, 0.0, lpPt, NPOLE);
	printf("APPROX: R+T=%5.12g,%5.12g\n\n", RC);

	RC = (RT[0]-RT[1]);
	printf("ACTUAL: R-T=%5.12g,%5.12g\n", RC);
	RC = polynom(eps, 0.0, 0.0, rnPt, ORD)*fapp_2(eps, 0.0, 0.0, lnPt, NPOLE);
	printf("APPROX: R-T=%5.12g,%5.12g\n\n", RC);

//------------------------------------------------------------------------------
// RESULT PROCESSING AND PREPARING SECTION
//------------------------------------------------------------------------------

	FILE * auxfile;
	auxfile = fopen("./auxfile.txt","w");
	fwrite(RT_vec, SCD, ida, auxfile);
	fclose(auxfile);


#ifdef MP
	MPI_Finalize();
#endif

	return 0;

}


cdouble funpoles_pos(cdouble z){
	cdouble RT[2];
//     	cylindrRT(z, r, al, BJPt, PQPt, RT, M);
//	return RT[0]+RT[1];
	return 1.0/cylindr(z, r, al, BJPt, PQPt, M, 'p');
//	return (z-94.0)/(z-95.0)/(z-95.0+I*2.0);

}	

double funmin(double * z){
	cdouble RT[2];
	cdouble zp = z[0]+I*z[1];
	for(int n=0;n<4;++n){
		zp*=zp;
	}
	#ifdef POSITIVE
		if(z[0] > 1.0){
			return -1.0/(cabs(cylindr(z[0]+I*z[1], r, al, BJPt, PQPt, M, 'p')));
		}else{
	     		cylindrRT(z[0]+I*z[1], r, al, BJPt, PQPt, RT, M);
			return -cabs(RT[0]+RT[1]);
		}
	#else
//		printf("%5.12e\n", cabs(cylindr(z[0]+I*z[1], r, al, BJPt, PQPt, M, 'n')));

//		return cabs(cylindr(z[0]+I*z[1], r, al, BJPt, PQPt, M, 'n'));

		if(z[0] > 3.2){
			return -1.0/(cabs(cylindr(z[0]+I*z[1], r, al, BJPt, PQPt, M, 'n')));
		}else{
	     		cylindrRT(z[0]+I*z[1], r, al, BJPt, PQPt, RT, M);
			return -cabs(RT[0]-RT[1]);
		}

	#endif
}	




