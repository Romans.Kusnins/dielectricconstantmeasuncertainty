#include<stdio.h>
#include<float.h>


int main(int argc, char ** argv){

	printf("%d\n", FLT_RADIX);

	printf("%d\n", FLT_MAX_EXP);
	printf("%d\n", FLT_MIN_EXP);
	printf("%d\n", FLT_MANT_DIG);

	printf("%d\n", DBL_MAX_EXP);
	printf("%d\n", DBL_MIN_EXP);
	printf("%d\n", DBL_MANT_DIG);

	printf("%2.16e\n", FLT_MAX);
	printf("%2.16e\n", FLT_MIN);

	printf("%2.16e\n", DBL_MAX);
	printf("%2.16e\n", DBL_MIN);

	return 0;

}
