import os


try:
	fp = open('runMonteCarlo.sh','r')
except IOError:
	print('Sorry, runMonteCarlo.sh does not exist.')
	exit()

buf = ''

for line in fp:
	if line.find('MPATH') > -1 and line.find('=') > -1:
		mpath = line[0:line.find('=')+1].strip() + os.getcwd() + '\n'
		buf = buf + mpath
	else:
		buf = buf + line

fp.close()


fp = open('runMonteCarlo.sh','w')
fp.write(buf)
fp.close()

#===========================================================================

try:
	fp = open('runPoleZero.sh','r')
except IOError:
	print('Sorry, runPoleZero.sh does not exist.')
	exit()

buf = ''

for line in fp:
	if line.find('MPATH') > -1 and line.find('=') > -1:
		mpath = line[0:line.find('=')+1].strip() + os.getcwd() + '\n'
		buf = buf + mpath
	else:
		buf = buf + line

fp.close()


fp = open('runPoleZero.sh','w')
fp.write(buf)
fp.close()

#===========================================================================

try:
	fp = open('runPoleZeroInt.sh','r')
except IOError:
	print('Sorry, runPoleZeroInt.sh does not exist.')
	exit()

buf = ''

for line in fp:
	if line.find('MPATH') > -1 and line.find('=') > -1:
		mpath = line[0:line.find('=')+1].strip() + os.getcwd() + '\n'
		buf = buf + mpath
	else:
		buf = buf + line

fp.close()


fp = open('runPoleZeroInt.sh','w')
fp.write(buf)
fp.close()

#===========================================================================

try:
	fp = open('runPolyCoeff.sh','r')
except IOError:
	print('Sorry, runPolyCoeff.sh does not exist.')
	exit()

buf = ''

for line in fp:
	if line.find('MPATH') > -1 and line.find('=') > -1:
		mpath = line[0:line.find('=')+1].strip() + os.getcwd() + '\n'
		buf = buf + mpath
	else:
		buf = buf + line

fp.close()


fp = open('runPolyCoeff.sh','w')
fp.write(buf)
fp.close()

