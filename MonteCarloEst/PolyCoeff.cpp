#include<stdio.h>
#include<complex.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<omp.h>

#include"PoleZero.h"

#ifdef MP
	#include<mpi.h>
#endif

const char path[100] 		= "./poles_zeros";
const char pathpoly[100] 	= "./poly_coeff";


const cdouble eps = eps_r*(1.0 + I*tan_d);


int main(int argc,char * argv[]){

	cdouble RT[2];

//--------------------------------------------------------------------------------------

	double EPSIM =  cimag(eps);

	for(int id=0; id<49; ++id){

		al = 0.51 + id*dal;

		POLES_OPEN(al)
		POLY_OPENW(al)

		for(NUM0=0; NUM0<481; ++NUM0){

			POLES_READ(al)

			r = NUM0*dr + 0.001;

			kor = 2.0*PI*al*r;

			besselj(0, kor, BJo, 2*M+1);
			bessely(0, kor, BYo, 2*M+1);

			besscyl_freq(al, PQPt, M, IP);
	
        		polyfit(EPSMIN, EPSMAX, EPSIM, r, al, BJPt, PQPt, lPt, rPt, M, NPT, NPOLE, ORD);

			POLY_WRITE_P(al)
			POLY_WRITE_N(al)

		}

		POLES_CLOSE
		POLY_CLOSE

	}

	return 0;

}	






