try:
	fp = open('parameters.dat','r')
except IOError:
	print('Sorry, the file does not exist.')
	exit()

param = dict()

for line in fp:
	if '=' in line:
		print(line, end='')
		id = line.find('=')
		if line[0:id].strip() == "APPROX":
			param[line[0:id].strip()] = line[id+1:].strip()
		else:
			param[line[0:id].strip()] = float(line[id+1:].strip())	
	else:
#		print('No equals sign.')
		pass
fp.close()

#for  key,val in param.items():
#	print(key,val)


buf = ""

if "eps_r" in param.keys():
	buf = buf + "const double \t eps_r \t = %1.8f;\n" % (param["eps_r"])
if "tan_d" in param.keys():
	buf = buf + "const double \t tan_d \t = %1.8f;\n\n" % (param["tan_d"])

if "APPROX" in param.keys():
	if param["APPROX"] == "yes":	
		buf = buf + "const bool \t approx  = %s;\n" % ("true")
	else:
		buf = buf + "const bool \t approx  = %s;\n" % ("false")

if "eps_min" in param.keys():
	buf = buf + "const double \t EPSMIN \t = %1.8f;\n" % (param["eps_min"])
if "eps_max" in param.keys():
	buf = buf + "const double \t EPSMAX \t = %1.8f;\n" % (param["eps_max"])


if "BNUM" in param.keys():
	buf = buf + "const int \t M  = %1d;\n\n" % (param["BNUM"])

if "ORD" in param.keys():
	buf = buf + "const int \t ORD  = %1d;\n\n" % (param["ORD"])

if "r_min" in param.keys():
	buf = buf + "const double \t r_min \t = %1.8f;\n" % (param["r_min"])
if "r_max" in param.keys():
	buf = buf + "const double \t r_max \t = %1.8f;\n\n" % (param["r_max"])

if "del_r" in param.keys():
	buf = buf + "const double \t del_r \t = %1.8f;\n\n" % (param["del_r"])
if "del_S11" in param.keys():
	buf = buf + "const double \t del_S11 \t = %1.8f;\n\n" % (param["del_S11"])

if "f" in param.keys():
	buf = buf + "const double \t f \t = %1.8f;\n" % (param["f"])
if "a" in param.keys():
	buf = buf + "const double \t a \t = %1.8f;\n" % (param["a"])
if "x_o" in param.keys():
	buf = buf + "const double \t x_o \t = %1.8f;\n\n" % (param["x_o"])

if "ACC" in param.keys():
	buf = buf + "const double \t ACC \t = %1.8f;\n" % (param["ACC"])
if "LEN" in param.keys():
	buf = buf + "const double \t LEN \t = %1.8f;\n" % (param["LEN"])
if "NUMIT" in param.keys():
	buf = buf + "const int \t NUMI \t = %1d;\n" % (param["NUMIT"])
if "TRIALS" in param.keys():
	buf = buf + "const int \t TRIALS  = %1d;\n" % (param["TRIALS"])

fp = open('MonteParam.h','w')

fp.write(buf)

fp.close()

