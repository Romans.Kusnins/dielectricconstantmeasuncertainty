#include<stdio.h>
#include<stdlib.h>

#include"MonteParam.h"


#define cdouble _Complex double

#define PI 3.141592653589793116

const int SCD = sizeof(cdouble);

//===============================================================================================

double r;

const int NPOLE = 54;
const int IP 	= 31;
const int NPT 	= 100;

double al = round(a*f/3.0)*0.01;

cdouble kor;

const double dr  = 0.001;
const double dal = 0.01; 

//===============================================================================================

#define POW2(x) ((x)*(x))


#define lpo(i) lpo[i-1]
#define lno(i) lno[i-1]

#define rbpo(i) rbpo[i-1]
#define rbno(i) rbno[i-1]


#define PRINT_POLY {for(m = 1; m <= ORD; ++m){printf("coef_p = (%5.15e,%5.15e) \n", rbp(m));}\
printf("\n");for(m = 1; m <= ORD; ++m){printf("coef_n = (%5.15e,%5.15e) \n", rbn(m));}}

#define PRINT_POLES(xp,xn) {for(numit=1; numit<=NE; ++numit){printf("eps = (%5.15f, %5.15f) \n", xp(numit));}\
printf("\n\n"); for(numit=1; numit<=NE; ++numit){printf("eps = (%5.15f, %5.15f) \n", xn(numit));}}


#define GEN_FILE_POLES(al) {sprintf(poles_p, "%s/poles_new_tr_%d_1_p", path, static_cast<int>((al)*100.0)); sprintf(poles_n, "%s/poles_new_tr_%d_1_n", path, static_cast<int>((al)*100.0));}
#define GEN_FILE_POLY(al)  {sprintf(polys_p, "%s/polys_new_tr_%d_1_p", pathpoly, static_cast<int>((al)*100.0)); sprintf(polys_n, "%s/polys_new_tr_%d_1_n", pathpoly, static_cast<int>((al)*100.0));}

#define OPENCHECK(fptr) if((fptr)==NULL){ printf("The file does not exist.\n"); exit(EXIT_FAILURE);}

#define POLES_OPEN(al)  {GEN_FILE_POLES(al); fpoles_p = fopen(poles_p,"r"); OPENCHECK(fpoles_p) \
			 		     fpoles_n = fopen(poles_n,"r"); OPENCHECK(fpoles_n)}

#define POLES_OPENW(al) {GEN_FILE_POLES(al); fpoles_p = fopen(poles_p,"r+");  fpoles_n = fopen(poles_n,"r+");}

#define POLY_OPEN(al)   {GEN_FILE_POLY(al);  fpolys_p = fopen(polys_p,"r");  fpolys_n = fopen(polys_n,"r");} 
#define POLY_OPENW(al)  {GEN_FILE_POLY(al);  fpolys_p = fopen(polys_p,"w");  fpolys_n = fopen(polys_n,"w");}

#define POLES_CLOSE    {fclose(fpoles_p); fclose(fpoles_n);}
#define POLY_CLOSE     {fclose(fpolys_p); fclose(fpolys_n);}


#define POLES_READ(al) {\
	fseek(fpoles_p, SCD*NUM0*NPOLE, SEEK_SET);\
	fseek(fpoles_n, SCD*NUM0*NPOLE, SEEK_SET);\
	fread(lpo, SCD, NPOLE, fpoles_p);\
	fread(lno, SCD, NPOLE, fpoles_n);}

#define POLES_WRITE(al) {\
	fseek(fpoles_p, SCD*NUM0*NPOLE, SEEK_SET);\
	fseek(fpoles_n, SCD*NUM0*NPOLE, SEEK_SET);\
	fwrite(lpo, SCD, NPOLE, fpoles_p);\
	fwrite(lno, SCD, NPOLE, fpoles_n);}

#define POLES_WRITE_P(al) {\
	fseek(fpoles_p, SCD*NUM0*NPOLE, SEEK_SET);\
	fwrite(lpo, SCD, NPOLE, fpoles_p);}

#define POLES_WRITE_N(al) {\
	fseek(fpoles_n, SCD*NUM0*NPOLE, SEEK_SET);\
	fwrite(lno, SCD, NPOLE, fpoles_n);}


#define POLES_READ_R(al) {\
	fseek(fpoles_p,SCD*NUM0*NPOLE,SEEK_SET);\
	fseek(fpoles_n,SCD*NUM0*NPOLE,SEEK_SET);\
	fread(lpo, SCD, NPOLE, fpoles_p);\
	fread(lno, SCD, NPOLE, fpoles_n);\
	fseek(fpoles_p,SCD*NUMR*NPOLE,SEEK_SET);\
	fseek(fpoles_n,SCD*NUMR*NPOLE,SEEK_SET);\
	fread(lpr, SCD, NPOLE, fpoles_p);\
	fread(lnr, SCD, NPOLE, fpoles_n);}

#define POLES_READ_A(al) {\
	fseek(fpoles_p,SCD*NUM0*NPOLE,SEEK_SET);\
	fseek(fpoles_n,SCD*NUM0*NPOLE,SEEK_SET);\
	fread(lpa, SCD, NPOLE, fpoles_p);\
	fread(lna, SCD, NPOLE, fpoles_n);}


#define POLY_WRITE_P(al) {\
	fseek(fpolys_p,SCD*NUM0*ORD,SEEK_SET);\
	fwrite(rpo, SCD, ORD, fpolys_p);}

#define POLY_WRITE_N(al) {\
	fseek(fpolys_n,SCD*NUM0*ORD,SEEK_SET);\
	fwrite(rno, SCD, ORD, fpolys_n);}

#define POLY_READ(al) {\
	fseek(fpolys_p,SCD*NUM0*ORD,SEEK_SET);\
	fseek(fpolys_n,SCD*NUM0*ORD,SEEK_SET);\
	fread(rpo, SCD, ORD, fpolys_p);\
	fread(rno, SCD, ORD, fpolys_n);}

#define POLY_READ_R(al) {\
	fseek(fpolys_p,SCD*NUM0*ORD,SEEK_SET);\
	fseek(fpolys_n,SCD*NUM0*ORD,SEEK_SET);\
	fread(rpo, SCD, ORD, fpolys_p);\
	fread(rno, SCD, ORD, fpolys_n);\
	fseek(fpolys_p,SCD*NUMR*ORD,SEEK_SET);\
	fseek(fpolys_n,SCD*NUMR*ORD,SEEK_SET);\
	fread(rpr, SCD, ORD, fpolys_p);\
	fread(rnr, SCD, ORD, fpolys_n);}

#define POLY_READ_A(al) {\
	fseek(fpolys_p,SCD*NUM0*ORD,SEEK_SET);\
	fseek(fpolys_n,SCD*NUM0*ORD,SEEK_SET);\
	fread(rpa, SCD, ORD, fpolys_p);\
	fread(rna, SCD, ORD, fpolys_n);}


//----------------------------------------------------------------------------------------

void     besselj(int, cdouble, cdouble *,int);
void     bessely(int, cdouble, cdouble *,int);
void     besscyl_freq(double, cdouble * PQ[], const int, const int);
void     besscyl_rad(cdouble eps, double r, double al, cdouble * BJ[],cdouble * gampt[], const int M);

void     Legendre(int, double*, double*);
cdouble  cylindr(cdouble, double, double, cdouble * BJPt[], cdouble * PQPt[],int,const char);
void     cylindrRT(cdouble, double, double, cdouble * BJPt[], cdouble * PQPt[], cdouble *,int);

void     polyfit(double, double, double, double, double, cdouble *BJPt[], cdouble *PQPt[],cdouble * lpt[],cdouble * rbpt[],const int,const int,const int, const int);

void     sortcr(cdouble *,const int);
void     poles(double, double, cdouble, cdouble *, int, int, int, int, int, const char);
void     nelder_mead(double (*) (double*), double *, double,double, const int);

cdouble  polynom(cdouble eps, double rd, double fd, cdouble * rbpt[], const int);
cdouble  fapp_2(cdouble eps,double rd,double fd,cdouble * epspt[],const int);
cdouble  fapp(cdouble eps,cdouble * eps_o,const int);
cdouble  fapp_2N(cdouble eps,double rd,double fd, cdouble * polPt[], const int);
void     prefapp_2(double rd, double fd, cdouble * epspt[],cdouble * polPt[], const int);

double   funmin_pos(double * z);
double   funmin_neg(double * z);

void	 pole_detect_update(cdouble (*fun)(cdouble), cdouble zo, double rad, cdouble * poles, double * ind, const int N);

void	 MoM_two_in_two_d(cdouble epo, double la, double ro, double xo, cdouble * RT, const int N);

//------------------------------------------------------------------------------------------------------------------

FILE *fpoles_p, *fpoles_n;
FILE *fpolys_p, *fpolys_n;

char poles_p[100], poles_n[100];
char polys_p[100], polys_n[100];

int NUM0, NUMR;

double	ph[IP], ww[IP];


#define ph(i) ph[i-1]
#define ww(i) ww[i-1]


typedef char flname[100];
typedef FILE  *FLPt;
 

cdouble pl;

cdouble   P1[M*M], P2[M*M];
cdouble   Q1[M*M], Q2[M*M];
cdouble * PQPt[] = {P1,P2,Q1,Q2};

cdouble   BJo[2*M+1], BYo[2*M+1];
cdouble * BJPt[] = {BJo, BYo};

cdouble   lpo[NPOLE], lno[NPOLE];
cdouble   lpr[NPOLE], lnr[NPOLE];
cdouble   lpa[NPOLE], lna[NPOLE];

cdouble * lPt[] = {lpo, lno};
cdouble * lpPt[] = {lpo, lpr, lpa};
cdouble * lnPt[] = {lno, lnr, lna};

cdouble   rpo[ORD], rno[ORD];
cdouble   rpr[ORD], rnr[ORD];
cdouble   rpa[ORD], rna[ORD];

cdouble * rPt[] = {rpo, rno};
cdouble * rpPt[] = {rpo, rpr, rpa};
cdouble * rnPt[] = {rno, rnr, rna};

//-------------------------------------------------------------

cdouble cpow(cdouble x, int ord){

	cdouble res = 1.0;

	for(int k=1; k<=ord; ++k){
		res *= x;
	}
	return res;
} 

