import numpy as np
from matplotlib import pyplot as plot


str = input('\n Please, specify the dielectric constant value: ')

if(len(str) < 1):
	print('\t The default value of 60 will be used.')
	ep = 60		# default value of the dielectric constant
else:
	ep = float(str)	

str = input('\n Please, specify the uncertainty in r: ')

if(len(str) < 1):
	print('\t The default value of 0.005 will be used.')
	dr = 0.005	# default value of the uncertainty in r
else:
	dr = float(str)

str = input('\n Please, specify the uncertainty in |S11|: ')

if(len(str) < 1):
	print('\t The default value of 0.01 will be used.')
	dS11 = 0.01	# default value of the uncertainty in |S11|
else:
	dS11 = float(str)

#------------------------------------------------------------
#  GENERATING THE FILE NAME
#------------------------------------------------------------

def dotrm(num, decpl, *opt):
	if len(opt) < 1:
		formt = "%1." + "%d" % (decpl) + "f"
		str = formt % (num)
		str = str.rstrip('0')
		id = str.find('.')
		str = str[0:id] + str[id+1:]
		return str
	else:
		formt = "%1." + "%d" % (decpl) + "f"
		str = formt % (num)
		str = str.replace('.', opt[0])
		return str

epstr	= dotrm(ep,2,'p')
drstr	= dotrm(dr,6)
dS11str = dotrm (dS11,6)

datstr  = "./DATA/ESTDATA_" + epstr +"_dr_" + drstr + "_dS11_" + dS11str + ".dat"
#------------------------------------------------------------------------


dat = np.fromfile(datstr, dtype=float)

LEN 	= int(len(dat)/5)

r 	= dat[0:LEN]
mnvl	= dat[LEN:2*LEN]
sig	= dat[2*LEN:3*LEN]


figstr  = epstr + "_dr_" + drstr + "_dS11_" + dS11str + ".png"


##plot.title("Standard uncertainty vs. rod radius")
##plot.xlabel("r, mm")
##plot.ylabel("u(\varpesilon_{\mathrm{r}}), %")

##plot.rcParams['text.usetex'] = True

fig1, ax1 = plot.subplots(figsize=(6,4), tight_layout=True)
ax1.plot(r, sig)
ax1.set_title("Relative uncertainty vs. rod radius")
ax1.set_xlabel(r'r, mm')
ax1.set_ylabel(r'Relative uncertainty, %')
ax1.grid()
#plot.show()
plot.savefig("./FIG/sigma_" + figstr)

fig2, ax2 = plot.subplots(figsize=(6,4), tight_layout=True)
ax2.plot(mnvl)
ax2.set_title("Mean value vs. rod radius")
ax2.set_xlabel(r'r, mm')
ax2.set_ylabel(r'Mean value')
ax2.grid()
#plot.show()
plot.savefig("./FIG/mean_" + figstr)

print("The following two files have been saved to ./FIG/: ")
print("\t 1. \t sigma_" + figstr)
print("\t 2. \t mean_" + figstr)
print()



