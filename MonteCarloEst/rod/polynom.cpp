#include<stdio.h>
#include<complex.h>

#define cdouble _Complex double

#define rb_o(i) rbpt[0][i-1]
#define rb_r(i) rbpt[1][i-1]
#define rb_f(i) rbpt[2][i-1]

#define RB(m)  ((rb_f(m) - rb_o(m))*fd + (rb_r(m) - rb_o(m))*rd + rb_o(m))


cdouble polynom(cdouble eps, double rd, double fd, cdouble * rbpt[], const int ORD){

int m;

cdouble res = RB(ORD);
cdouble tmp = 1.0;

for(m=1; m< ORD; ++m){
    tmp *= eps; 
    res += RB(ORD-m)*tmp;   
}

return res;

} 
