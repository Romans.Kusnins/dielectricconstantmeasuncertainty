#include<stdio.h>
#include<complex.h>

#define cdouble _Complex double

#define eps_o(i) epspt[0][i-1]
#define eps_r(i) epspt[1][i-1]
#define eps_f(i) epspt[2][i-1]

#define EPSIL(m)   ((eps_f(m)-eps_o(m))*fd + (eps_r(m)-eps_o(m))*rd + eps_o(m))

cdouble fapp_2(cdouble eps,double rd,double fd, cdouble * epspt[], const int M){

int m;

cdouble res=1.0;
cdouble epsd;

for(m=1;m<=(M-0);++m){
    
    epsd = EPSIL(m);
    res *= (eps - conj(epsd))/(eps - epsd);   

}

return res;

} 
