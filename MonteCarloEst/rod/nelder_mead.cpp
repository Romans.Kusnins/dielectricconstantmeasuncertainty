
#include<stdio.h>
#include<complex.h>
#include<math.h>


#define cdouble _Complex double 

#define PI 3.141592653589793116

#define POW2 ((x)*(x))

#define tmp(i) tmp[i-1]

#define FOR_CYCLE(K) for(int k = 1; k <= K; ++k)


#define VECT_INIT(xo, dxo, del)  {xo(1) = vect(dxo);  tmp(1) = dxo(1) + del;  tmp(2) = dxo(2);\
                                  xo(2) = vect(tmp);  tmp(1) = dxo(1);        tmp(2) = dxo(2) + del;\
                                  xo(3) = vect(tmp); }

#define FUN_INIT(fun,xo,fv)    FOR_CYCLE(3){ fv(k) = fun(xo(k)); }


#define CENTROID(xo,n,xc)      {xc = 0.0; FOR_CYCLE(3){ if(k != n ){ xc += xo(k); }} xc *= 0.5;}

#define ALL_CENTROID(xo,xc)    {xc = 0.0; FOR_CYCLE(3){ xc += xo(k); } xc *= (1.0/3.0);}



#define a_r      2
#define a_e      3
#define a_sh_r   0.5
#define a_sh_w   0.5
#define a_red    0.5


#define REFLECT(xc,xw)   (   a_r*((xc) - (xw)) + (xw))
#define EXPAND(xc,xw)    (   a_e*((xc) - (xw)) + (xw))
#define SHRINK_R(xc,xr)  (a_sh_r*((xr) - (xc)) + (xc))
#define SHRINK_W(xc,xw)  (a_sh_w*((xw) - (xc)) + (xc))

#define REDUCE(xb,idb,xo)    FOR_CYCLE(3){if(k != idb){ xo(k) = a_red*((xo(k))-(xb)) + (xb); }}



void sortr(double *,int *,int);

#define fun(x) dfun((double *) &x)



class vect{

public:

double x[2];

vect(double * y){
    x[0] = y[0];   
    x[1] = y[1];   
}
vect(double a, double b){
    x[0] = a;   
    x[1] = b;   
}
vect(double y){
    x[0] = y;   
    x[1] = y;   
}
vect()
{
    x[0] = 0.0;   
    x[1] = 0.0;   
}

~vect()
{
}

vect& operator= (const double& y){
    this->x[0] = y;
    this->x[1] = y;
}

};



vect operator+ (const vect& x, const vect& y)
{
vect z;
z.x[0] = x.x[0] + y.x[0];
z.x[1] = x.x[1] + y.x[1];
return z;
} 

void operator+= (vect& x, const vect& y)
{
x.x[0] += y.x[0];
x.x[1] += y.x[1];
} 


void operator*= (vect& x, const double& alpha)
{
x.x[0] *= alpha;
x.x[1] *= alpha;
} 

vect operator- (const vect& x, const vect& y)
{
vect z;
z.x[0] = x.x[0] - y.x[0];
z.x[1] = x.x[1] - y.x[1];
return z;
} 

vect operator* (const double& alpha, const vect& x)
{
vect z;
z.x[0] = alpha*x.x[0];
z.x[1] = alpha*x.x[1];
return z;
} 

vect operator* (const vect& x, const double& alpha)
{
vect z;
z.x[0] = alpha*x.x[0];
z.x[1] = alpha*x.x[1];
return z;
} 


#define  dxo(i) dxo[i-1]
#define  xo(i)  xo[i-1]
#define  fv(i)  fv[i-1]
#define  id(i)  id[i-1]


#define  xw   xo(id(3))
#define  xsw  xo(id(2))
#define  xb   xo(id(1))

#define  idw  id(3)
#define  idsw id(2)
#define  idb  id(1)

#define  fw   fv(id(3))
#define  fsw  fv(id(2))
#define  fb   fv(id(1))



void nelder_mead(double (*dfun)(double *), double * dxo, double del, double acc, const int M)
{

int n,  id[3] = {1,2,3}, itnum = 0;
double  tmp[2];
vect    xo[3], xr, xe, xc;
double  fv[3], fr, fe;

VECT_INIT(xo, dxo, del);
FUN_INIT(fun, xo, fv);


sortr(fv,id,3);

CENTROID(xo, idw, xc);

xr = REFLECT(xc,xw);
fr = fun(xr);

//printf("%5.12f,%5.12f \n",xr.x[0],xr.x[1]);

for(n=1;n<=M;++n){

/*
printf("iter = %d\n", n);
printf("xo(1) = %5.12f,%5.12f\n",xo(1).x[0],xo(1).x[1]);
printf("xo(2) = %5.12f,%5.12f\n",xo(2).x[0],xo(2).x[1]);
printf("xo(3) = %5.12f,%5.12f\n\n",xo(3).x[0],xo(3).x[1]);
printf("fw  = %5.12e\n",   fw);
printf("fsw = %5.12e\n",   fsw);
printf("fb  = %5.12e\n\n", fb);
printf("fr  = %5.12e\n\n", fr);
*/
 
   if(fr < fb){
        xe = EXPAND(xc,xw);  
        fe = fun(xe);
      if(fe < fr){
        xw = xe;
        fw = fe;
      }else{
        xw = xr;
        fw = fr;
      }
   }else if(fr > fsw){
     if(fr < fw){
          xe = SHRINK_R(xc,xr);
          fe = fun(xe);
        if(fe < fr){
          xw = xe;
          fw = fe;  
        }else{
          REDUCE(xb,idb,xo);
          ALL_CENTROID(xo,xc);
          FOR_CYCLE(3){fv(k) = fun(xo(k));} 
        }
     }else{
          xe = SHRINK_W(xc,xw);
          fe = fun(xe);
        if(fe < fw){
          xw = xe;
          fw = fe;
        }else{
          REDUCE(xb,idb,xo);
          ALL_CENTROID(xo,xc);
          FOR_CYCLE(3){fv(k) = fun(xo(k));} 
        }
     }
   }else{
     xw = xr;   
     fw = fr; 
   } 

sortr(fv,id,3);

CENTROID(xo, idw, xc);

xr = REFLECT(xc, xw);
fr = fun(xr); 

if(fabs(xc.x[0]-xr.x[0]) + fabs(xc.x[1]-xr.x[1])  < acc){
   break;
}

}


//ALL_CENTROID(xo,xc);

//printf("itnum = %d \n",itnum);

//FOR_CYCLE(2){ dxo(k) = xc.x[k-1];}

dxo(1) = xb.x[0];
dxo(2) = xb.x[1];

}
