#include<stdio.h>
#include<math.h>
#include<omp.h> 
#include<complex.h>


#define PI 3.141592653589793238462

#define cdouble  _Complex double


void minseek(double (*objfun)(double*), double *xin, double len, double acc, const int maxit){ 

	
    double xl[2], xr[2], fxl, fxr, fo, xo[2];   

    xo[0] = xin[0];
    xo[1] = xin[1];	 	

    len*=0.5;	

    xl[0] = xo[0] - len;  
    xr[0] = xo[0] + len;
    xl[1] = xo[1];  
    xr[1] = xo[1];
 
    fxl = objfun(xl);
    fxr = objfun(xr);           
    fo  = objfun(xo);           

    for(int i=0;i<=maxit;++i){        
        
      if((fxl < fo) && (fxl < fxr)){        
            xr[0] = xo[0];
            xo[0] = xl[0];
            xl[0] = xo[0] - len;
	                            
            fxr = fo;
            fo = fxl;
           
            fxl = objfun(xl);                               
                                                  
      }else if(fxr<fo && fxr<fxl){
                            
            xl[0] = xo[0];
            xo[0] = xr[0];
            xr[0] = xo[0] + len;
	                                        
            fxl = fo;
            fo = fxr;
                        
            fxr = objfun(xr);

	}else{

	    len *= 0.5;	
                            
            xl[0] = xo[0] - len;
            xr[0] = xo[0] + len;
            
            fxl = objfun(xl);
            fxr = objfun(xr);

	}
	if(len < acc){            
 	   break;	
	}           
    }        
    xin[0] = xo[0];
    xin[1] = xo[1];
}



